package tablePackage;

import java.awt.*;
import java.util.Scanner;
import java.util.function.IntBinaryOperator;

public class TableGeneral {
    //PROGRAMME DESCRIPTION : output d'une table de multiplication avec un canvas interne à choix pour blanches

    //declaring global variables
    //not possible etendre le scope de l' instancement in main program
    //not possible to instance of the same class currency outside de methods fields
    //
    //
    //Scanner inp = new Scanner(System.in);
    //TableGeneral tablegeneral = new TableGeneral();
    Scanner inp;
    public int dim1;
    public int an=2022;
    public int limSup;
    int A[][];
    int B[][];
    //starting program
    public static void main (String Args[]){
        try {
            //on note bien que d'une inner class il faut instancier la
            //external class et after la inner class:
            TableGeneral tablegeneral = new TableGeneral();
            //on note bien que d'une inner class il faut instancier la
            //external class et after la inner class:
            TableGeneral.TableSpecial genspe = tablegeneral.new TableSpecial();

            //et inner of inner class TableSpecial
            TableGeneral.TableSpecial.Iteration genspeite = genspe.new Iteration();
            //pour eviter de se referer à la classe tablegeneral toujours
            //et donc le assigner avant l'instancement des inners class, il vais falloir
            //rendre static les variable an et dim1; pas conseillé si vous voulez
            //totalement par Objet Programming construir le programme
            System.out.println("in main program tha value an:_" + tablegeneral.an);
            tablegeneral.inp = new Scanner(System.in);
            System.out.println("1-Enter la bidimention of table da uno in su:_");
            tablegeneral.dim1 = tablegeneral.inp.nextInt();
            System.out.println("2-Enter la limite superieur blanche de la table da zero a :_"+(tablegeneral.dim1));
            tablegeneral.limSup = genspe.controlSaisi(tablegeneral.inp.nextInt(),tablegeneral.dim1);


            //instancement of A array objet bidimentional
            tablegeneral.A=new int[tablegeneral.dim1][tablegeneral.dim1];

            //calling function genspe.calcule of inner class TableSpecial as parameter of subroutine (methode) tablegeneral.outConsole
            //of external class TableGeneral
            tablegeneral.outConsole(genspe.calcule(tablegeneral.limSup,tablegeneral.dim1,tablegeneral.A));
            //calling function of external class
            tablegeneral.outin(tablegeneral.an);
            //calling function of external class
            //in parameter a multidimentional array B , output de la function genspe.calcule
            ////tablegenerale.outConsole(genspe.calcule(tablegeneral.limSup,tablegeneral.dim1,tablegeneral.A));

        }catch (Exception e){
            System.out.println("Error in main founded type:_"+e);
        }
    }
    //subroutine (methode) experimentale
    public void outin(int an){
        an=an-621;
        System.out.println("in_subroutine_outin_this subroutine is calling from out class and an is:_"+an);
        TableGeneral tablegeneral = new TableGeneral();
    }
    ////subroutine (methode) pour le printing in console de l'array A
    public void outConsole(int[][] D){
        try{

            //currenty class et after la inner class:
            TableGeneral tablegeneral = new TableGeneral();
            //inner istanciation ordinaire inner class TableSpecial
            TableGeneral.TableSpecial genspe = tablegeneral.new TableSpecial();
            //et inner of inner class TableSpecial
            TableGeneral.TableSpecial.Iteration genspeite = genspe.new Iteration();




            int i=-1;
            int j;
            System.out.println("----------------------------------------------------------");
            do{
                i=i+1;
                j=-1;
                genspeite.iterationVisualise(i, j, D);
                //to print in next line
                System.out.println();
            }while(i<D.length-1);

            System.out.println("----------------------------------------------------------");

        }catch (Exception e){
            System.out.println("Error founded in outConsole subroutine"+e);
        }finally{
            System.out.println("PROGRAM TABLE FINISHED");
        }
    }




    //inner class
    public class TableSpecial {
        //la function returne an Array B after calcule numerique
        //sur table multiplicative
        //attention: it's only for une specifique fields Scanner inp , present in external class TableGeneral
        //if not you not need to instanciate the external class to beginning
        //you can starting to inner instantiation from currenty class TableSpecial
        //cette instance d'external class TableGeneral sera l'instance commune a tout fields subroutine and functions
        //(sauf main program)
        //l'unique instance possible à l'exterieurs des fiels subroutine ou function
        //est la classe externe à l'inner class
        TableGeneral tablegeneral = new TableGeneral();


        public int[][] calcule(int limSup, int dimension, int B[][]) {
            try{
            int i = -1;
            int j = -1;
            System.out.println("Vous avez choisi la limite in bianco di:__" + limSup + "__su una dimensione di tabella di:__" + dimension);
            //inner istanciation ordinaire inner class TableSpecial
            TableGeneral.TableSpecial genspe = tablegeneral.new TableSpecial();
            //et inner of inner class TableSpecial
            TableGeneral.TableSpecial.Iteration genspeite = genspe.new Iteration();

            //you can to became to TableSpecial, et le reste des inner classes,
            //sans retourner en TableGenerale external class if not necessary
            //TableSpecial genspe = new TableSpecial();
            //TableSpecial.Iteration genspeite = genspe.new Iteration();
            ////
            //on note bien une inner d'inner class instanced after de instanciation of external classes TableSpecial but
            //sans retourner en external class TableGeneral, if not necessairy of any field subroutine or functions, with:
            //TableSpecial genspe = tablegeneral.new TableSpecial(); that is not pertinent

            //
            do {
                i = i + 1;

                j = -1;
                    //the objet genspeite existe bien and the access to his
                    //subroutines and function (methods) fields it' possible !
                    genspeite.iterationInner(i, j, limSup, dimension, B);

                } while (i < dimension-1);
            //System.out.println("in subroutine output_the value anno an is:_" + B);

            }catch (Exception e){
                System.out.println("Error in main founded type:_"+e);
            }finally {
                System.out.println("ok for function calcul");
            }
            return B;
        }

        public int controlSaisi(int value, int dimValue) {
            TableGeneral tablegeneral = new TableGeneral();
            int limSup1 = 0;
            int i = -1;
            do {
                i = i + 1;
                if (value <= dimValue) {
                    limSup1 = value;
                    return (limSup1);
                } else {
                    System.out.println("Enter a value inferiore or equal to_" + dimValue);
                    tablegeneral.inp = new Scanner(System.in);
                    value = tablegeneral.inp.nextInt();
                }
            } while (i < 5);
            return (limSup1);
        }

        //inner d'inner class
        public class Iteration {

            public void iterationInner(int i, int j, int limSup, int dimentionInner, int C[][]) {
                //function for inner iteration of calcule numerical
                try {

                    do {
                        j = j + 1;
                        if(j<=limSup-1 && i<=limSup-1){
                            //element empty
                            C[i][j]='\u0000';
                        }else{
                            //element i-eme full
                            C[i][j]=(i+1)*(j+1);
                        }
                    } while (j < dimentionInner-1);
                    }catch (Exception e){
                    System.out.println("Error found:_"+e);
                }finally{
                    System.out.println("----");
                }
            }

            public void iterationVisualise(int i, int j, int E[][]){
                String concaString;
                String format="%5d";
                String format1="%5s";
                //while(D.lenght); not need to passing in parameter the dimention you are in multidimentional array the dimentions
                do{
                    j=j+1;
                    if (E[i][j]=='\u0000') {
                        // \u0000 is zero integer scalar
                        System.out.format(format1, "");
                    }else{
                        System.out.format(format, E[i][j]);
                    }
                }while (j<E.length-1);
                //E.lenght to output elements lim sup first dimention
                //E[1].lenght to output elements lim sup second dimention
                //in this case it's the same
            }
        }
    }
}
